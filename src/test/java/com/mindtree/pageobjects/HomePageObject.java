package com.mindtree.pageobjects;

import org.openqa.selenium.WebDriver;

import com.mindtree.reusableMethods.OpenUrl;
import com.mindtree.reusableMethods.WebDriverHelper;

public class HomePageObject {
	WebDriver driver;
	WebDriverHelper driverHelperObj = new WebDriverHelper();
	OpenUrl openUrlObj = new OpenUrl();

	public void openAppUrl() {
		driver = openUrlObj.openAppURL();
		String pageTitle = driver.getTitle();
		System.out.println("The page title is: "+pageTitle);
	}

}
