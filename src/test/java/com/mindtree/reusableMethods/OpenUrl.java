package com.mindtree.reusableMethods;

import org.openqa.selenium.WebDriver;

public class OpenUrl {
	
	WebDriverHelper driverHelperObj = new WebDriverHelper();
	WebDriver driver;
	
	public WebDriver openAppURL() {
		driver = driverHelperObj.initializeDriver();
		driver.get("https://www.google.com");
		return driver;
	}
	
}
